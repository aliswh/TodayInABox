package com.as.todayinabox;

import android.content.Context;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class RetrieveData {
    /**
     * Variables for getting Wikipedia data
     */
    private String wikiurl = "https://en.wikipedia.org/wiki/";
    URL url;
    {
        try {
            url = new URL(wikiurl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
    private String date = "";
    private String locale = Locale.getDefault().getLanguage();
    Context mContext;

    /**
     * Constructor gets context from main to retrieve system language
     * to set the right url for the wiki page
     * TODO each language should be tested
     * (test just 3 for now - english, italian, french)
     * @param context
     */
    public RetrieveData(Context context){
        this.mContext = context;
    }

    private String getDay(){
        Date d = Calendar.getInstance().getTime();
        SimpleDateFormat newFormat = new SimpleDateFormat("MMM d", Locale.forLanguageTag(locale));
        String formattedDate = newFormat.format(d);
        String[] dateSplit = formattedDate.split(" ");
        String month = dateSplit[0];
        String day = dateSplit[1];
        return date = month + "_" + day;
    }

    private String setURL(){
        return wikiurl = "https://" + Locale.forLanguageTag(locale) + ".wikipedia.org/wiki/" + date;
    }
}
