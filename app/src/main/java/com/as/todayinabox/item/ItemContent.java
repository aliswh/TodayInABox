package com.as.todayinabox.item;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing content for user interfaces
 */
public class ItemContent {

    /**
     * An array of sample items.
     */
    public static final List<ItemData> ITEMS = new ArrayList<ItemData>();

    /**
     * A map of sample items, given by ID.
     */
    public static final Map<String, ItemData> ITEM_MAP = new HashMap<String, ItemData>();

    private static final int COUNT = 25;



    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createItemData(i));
        }
    }

    private static void addItem(ItemData item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static ItemData createItemData(int position) {
        return new ItemData(String.valueOf(position), "Item " + position, giveDetails(position));
    }

    private static String giveDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    /**
     * Item representing a piece of content.
     */
    public static class ItemData {
        public final String id;
        public final String content;
        public final String details;

        public ItemData(String id, String content, String details) {
            this.id = id;
            this.content = content;
            this.details = details;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
